package com.example.alecp.trivial

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val playBtn = findViewById<Button>(R.id.playButton)
        playBtn.setOnClickListener { NextActivity() }

    }

    private fun NextActivity() {


        val editText = findViewById<EditText>(R.id.editTextNickname)
        if (editText != null) {
            val nicknameText = editText
            Toast.makeText(this, nicknameText.text, Toast.LENGTH_LONG).show()
        }
        val mainGame = Intent(this, Main2Activity::class.java)
        startActivity(mainGame)
    }
}
