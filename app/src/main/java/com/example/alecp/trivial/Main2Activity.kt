package com.example.alecp.trivial

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import kotlinx.android.synthetic.main.activity_main2.*
import org.json.JSONArray
import org.json.JSONException
import android.util.Log
import android.widget.*
import okhttp3.OkHttpClient
import okhttp3.Request
import android.net.ConnectivityManager
import java.util.*


@Suppress("DEPRECATION")

class Main2Activity : AppCompatActivity() {

    lateinit var context : Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        context = this
        getQuestions().execute()
    }

    internal inner class getQuestions : AsyncTask<Void, Void, String>() {

        lateinit var progressDialog : ProgressDialog
        var hasInternet = false

        override fun onPreExecute() {

            super.onPreExecute()
            progressDialog = ProgressDialog(context)
            progressDialog.setMessage("Downloading Questions...")
            progressDialog.setCancelable(false)
            progressDialog.show()

        }

        override fun doInBackground(vararg p0: Void?): String {
            if(isNetworkAvailable()){
                hasInternet = true
                val client = OkHttpClient()
                val url = "https://opentdb.com/api.php?amount=50"
                val request = Request.Builder().url(url).build()
                val response = client.newCall(request).execute()
                return response.body()?.string().toString()
            }else
                {
                   return "There was an error"
                }
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            progressDialog.dismiss()

            if(hasInternet)

                try{

                    val resultArray = JSONArray(result)
                }catch (e: JSONException)

        }

    }
}
